using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaGirlController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    public int isLive = 1;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isLive == 0)
        {
            animator.SetInteger("Estado", 2);
            
            rb.velocity = new Vector2(0, rb.velocity.y);
        }else
        {
            animator.SetInteger("Estado", 0);
            
            rb.velocity = new Vector2(5, rb.velocity.y);

            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.AddForce(Vector2.up * 30, ForceMode2D.Impulse);
                animator.SetInteger("Estado",1); 
            }  
        }
         
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        
        if (collision.gameObject.CompareTag("enemy"))
        {
            isLive = 0;
        }
       

    }
}